public class FileSystem {
    private Directory root;

    public FileSystem() {
        this.root = new Directory("root");
    }

    public void tree() {
        root.display();
    }

    // 2b
//    public FileSystemComponent copy(FileSystemComponent component) {
//        // call either copyFile of copyDirectory based on component type
//    }

    // 2b
//    private File copyFile(File file) {
//    }

    // 2b
//    private Directory copyDirectory(Directory directory) {
//    }
}

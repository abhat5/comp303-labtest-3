public class EncryptionDecorator {
    // implement


    // call the encryptionHelper wherever required
    // you may modify the code as required
    // For the purpose of the lab test, you just
    // need to print the file/directory
    // that you are trying to encrypt
    // you may also copy the code and paste it elsewhere
    private void encryptionHelper(String componentName) {
        System.out.println("Encrypting: " + componentName);
    }
}
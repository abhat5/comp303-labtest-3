public class CompressionDecorator {
    // implement


    // call the compressionHelper wherever required
    // you may modify the code as required
    // For the purpose of the lab test, you just
    // need to print the file/directory
    // that you are trying to compress
    // you may also copy the code and paste it elsewhere
    private void compressionHelper(String componentName) {
        System.out.println("Compressing: " + componentName);
    }
}
